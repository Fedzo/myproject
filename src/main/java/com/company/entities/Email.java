package com.company.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
public class Email implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "email")
    private String singlEmail;

    @Column(name = "date")
    private Date date;

    @Column(name = "description")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSinglEmail() {
        return singlEmail;
    }

    public void setSinglEmail(String singlEmail) {
        this.singlEmail = singlEmail;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
