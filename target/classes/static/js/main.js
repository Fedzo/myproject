const backToTopButton = document.querySelector(".btn-btn-circle");

window.addEventListener("scroll", scrollfunction);

function scrollfunction() {
    if(window.pageYOffset > 300) {
        backToTopButton.style.display = "block";
    }
    else {
        backToTopButton.style.display = "none";
    }
}